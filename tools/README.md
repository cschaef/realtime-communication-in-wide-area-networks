## Install OMNeT++ on Fedora
```bash
$ sudo dnf install make gcc gcc-c++ bison flex perl python2 tcl-devel tk-devel libxml2-devel zlib-devel doxygen graphviz qt5-qtbase qt5-qtbase-devel python-pandas python-posix_ipc python-matplotlib python-numpy OpenSceneGraph OpenSceneGraph-devel osgearth-devel
$ cd omnetpp-5.6.2
$ . setenv
$ ./configure
$ make
```

## Set up simulation environment
In order to recreate the simulation environment please apply following patches _in_ the NeSTiNg repository:
```bash
$ cd nesting
$ git apply 0001-adapt-nesting-source.patch
$ git apply 0002-adapt-nesting-simulation.patch
```

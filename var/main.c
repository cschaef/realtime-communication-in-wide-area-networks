/**
 * @file 	main.c
 * @author 	Christian Schaefer <cschaefer.itsb-b2018@fh-salzburg.ac.at>
 * @version	0.1
 * @date	16.09.2020
 * 
 * gcc main.c -o main -Wall
 * ./main --method="send" --interface="virbr0" --hwaddress="52:54:00:17:7e:b2"
 *
 */ 


#include <net/ethernet.h>
#include <net/if.h>
#include <netinet/ether.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <syslog.h>


#define PROGRAM_NAME "main"
#define PROGRAM_VERSION "0.1"
#define HWADDRSIZE 18
#define MAXPAYLOAD 1024


void print_version(void)
{
	printf("%s version %s\n", PROGRAM_NAME, PROGRAM_VERSION);
}

void print_help(void)
{
	printf("\n Usage: %s [options]\n", PROGRAM_NAME);
	printf("   Options: \n");
	printf("    -i, --interface=IFACE	   Name of the interface the programm should use to send or receive.\n");
	printf("    -h, --help			   Prints this help.\n");
	printf("    -H, --hwaddress=MACADDRESS	   Mac Address of the receiver eg. 00:11:22:33:44:55\n");
	printf("    -m, --method=SEND|RECEIVE	   Send to another program or listen forincoming connection.\n");
	printf("    -p, --payload=HEX Message	   Payload message in hex to be sent");
	printf("    -v, --version		   Print program version\n");
	printf("\n");
}

void generate_frame(struct ether_header *eh, struct ifreq if_mac, const uint8_t hwaddress[6])
{
	// send sender address
	eh->ether_shost[0] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[0];
	eh->ether_shost[1] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[1];
	eh->ether_shost[2] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[2];
	eh->ether_shost[3] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[3];
	eh->ether_shost[4] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[4];
	eh->ether_shost[5] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[5];
	
	// send destination address
	eh->ether_dhost[0] = hwaddress[0];
	eh->ether_dhost[1] = hwaddress[1];
	eh->ether_dhost[2] = hwaddress[2];
	eh->ether_dhost[3] = hwaddress[3];
	eh->ether_dhost[4] = hwaddress[4];
	eh->ether_dhost[5] = hwaddress[5];

	// set ethertype 
	eh->ether_type = htons(0x88AB);
	// eh->ether_type = htons(ETH_P_IP);
}

void prepare_dest_socket_address(struct sockaddr_ll *socket_address, struct ifreq* if_idx, const uint8_t hwaddress[6])
{
	socket_address->sll_ifindex = if_idx->ifr_ifindex;
	/* Address length*/
	socket_address->sll_halen = ETH_ALEN;
	/* Destination MAC */
	socket_address->sll_addr[0] = hwaddress[0];
	socket_address->sll_addr[1] = hwaddress[1];
	socket_address->sll_addr[2] = hwaddress[2];
	socket_address->sll_addr[3] = hwaddress[3];
	socket_address->sll_addr[4] = hwaddress[4];
	socket_address->sll_addr[5] = hwaddress[5];
}

void get_interface_index(struct ifreq* if_idx, int sockfd, const char* iface)
{
	memset(if_idx, 0, sizeof(struct ifreq));
	strncpy(if_idx->ifr_name, iface, IFNAMSIZ - 1);
	if(ioctl(sockfd,SIOCGIFINDEX, if_idx) < 0) 
		perror("SIOCGIFINDEX");
}

void get_interface_mac(struct ifreq* if_mac, int sockfd, const char* iface)
{
	memset(if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac->ifr_name, iface, IFNAMSIZ - 1);
	if (ioctl(sockfd, SIOCGIFHWADDR, if_mac) < 0)
	    perror("SIOCGIFHWADDR");
}

void convert_hw_address(uint8_t *res, char* hwaddress)
{
	uint8_t i = 0;
	const char delimiter[2] = ":";
	char* block = strtok(hwaddress, delimiter);
	while(block != NULL) {
		//res[i++] = (uint8_t)atoi(block);
		res[i++] = (unsigned char)strtol(block, (char **) NULL, 16);
		block = strtok(NULL, delimiter);
	}
}

void print_socket_address(struct sockaddr_ll socket_address)
{
	for(int i = 0; i < 6; i++)
	{
		printf("%02x", socket_address.sll_addr[i]);
		if(i < 5) printf(":");
	}
	printf("\n");
}

void print_payload(const char* payload, size_t payload_len)
{
	for(size_t i = 0; i < payload_len; i++) {
		printf("%x ", payload[i]);
	}
	printf("\n");
}

int send_msg(const char* hwaddress, const char* payload, size_t payload_len, const char* iface)
{
	printf("Sending to <%s> from <%s>\n", hwaddress, iface);
	
	int sockfd;
	uint8_t ihwaddress[6];
	struct ifreq if_idx;
	struct ifreq if_mac;
	struct sockaddr_ll socket_address;
	struct ether_header *eh = (struct ether_header *) payload;
	print_payload(payload, payload_len);
	int tx_len = sizeof(struct ether_header) + payload_len;
	printf("%d\n", tx_len);

	if ((sockfd = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1) {
		printf("Failure while creating socket!\n");
		return 1;
	}

	get_interface_index(&if_idx, sockfd, iface);
	get_interface_mac(&if_mac, sockfd, iface);
	convert_hw_address(ihwaddress, (char*) hwaddress);
	generate_frame(eh, if_mac, ihwaddress);
	prepare_dest_socket_address(&socket_address, &if_idx, ihwaddress);

	if (sendto(sockfd, payload, tx_len, 0, (struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll)) < 0) {
		printf("Send failed\n");
		return 1;
	} 

	return 0;
}

int receive_msg(const char* iface) 
{
	printf("Receiving from <%s>\n", iface);
	return EXIT_SUCCESS;
}

int address_valid(const char* hwaddress)
{
	return 1;
}

int main(int argc, char **argv) 
{
	char ifName[IFNAMSIZ];
	char hwAddress[HWADDRSIZE] = "";
	size_t payload_size = 5;
	char payload[] = { 0x00, 0x00, 0x00, 0x00, 0x00 };
	// char payload[] = { 0xaa, 0xbb, 0xcc, 0xdd, 0xee };
	int option_index = 0;
	int option = 0;
	int ret = 0;
	bool sender = false;

	// openlog(PROGRAM_NAME, LOG_PID | LOG_CONS, LOG_LOCAL0);
	// syslog(LOG_INFO, "started %s v%s", PROGRAM_NAME, PROGRAM_VERSION);
	
	static struct option long_options[] = {
		{"interface", 	required_argument,	0, 'i'},
		{"help", 	no_argument, 		0, 'h'},
		{"hwaddress", 	required_argument, 	0, 'H'},
		{"method", 	required_argument, 	0, 'm'},
		{"payload", 	required_argument,	0, 'p'},
		{"version", 	no_argument, 		0, 'v'},
		{NULL, 0, 0, 0}
	};

	while((option = getopt_long(argc, argv, "i:hH:m:v", long_options, &option_index)) != -1) {
		switch(option) {
		case 'i':
			strncpy(ifName, optarg, IFNAMSIZ);
			break;
		case 'h':
			print_help();
			return EXIT_SUCCESS;
		case 'H':
			strncpy(hwAddress, optarg, HWADDRSIZE);
			break;
		case 'm':
			if (strncmp("send", optarg, 4) == 0) 
				sender = true;
			break;
		case 'v':
			print_version();
			return EXIT_SUCCESS;
			break;
		default:
			print_help();
			return EXIT_FAILURE;
		}
	}

	if(sender && address_valid(hwAddress))
		ret = send_msg(hwAddress, payload, payload_size, ifName);
	else 
		ret = receive_msg(ifName);
	// closelog();
	return ret;
}
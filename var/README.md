# Using _netem_ to emulate WAN characteristics on a local interface
_netem_ provides Network Emulation functionality for testing protocols by emulating the properties of wide area networks. The current version emulates variable delay, loss, duplication and re-ordering. [1]  
This could also be used on a linux device with two ethernet interfaces which would act as a switch between two end devices.

## Network Scenario

        +----------+                                    +----------+
        |          |------+   WAN simulated NW   +------|          |
        |  Sender  | eth0 | -------------------> | eth0 | Receiver |
        |          |------+                      +------|          |
        +----------+  |                                 +----------+
                      |
                      +-----> Delay Generator on Iface with tc-netem [2]
## Install 
``$ apt-get update && apt-get upgrade ``  
``$ apt-get install iproute2``   
``$ apt-get install tcpdump ``   

## Various 
``$ tc qdisc add dev eth0 root netem delay 100ms `` - add 100ms delay  
``$ tc qdisc change dev eth0 root netem delay 100ms 10ms `` - change to 100ms delay with +/- 10ms variation  
``$ tc qdisc change dev eth0 root netem loss 0.1% `` - add 0.1% packet loss

## Monitor incoming traffic on an interface with mac filter for defined address
`` $ tcpdump -nettti eth0 '(ether dst host 52:54:00:56:c0:3c)'`` 

## More Information
[1] https://wiki.linuxfoundation.org/networking/netem  
[2] https://www.man7.org/linux/man-pages/man8/tc-netem.8.html    
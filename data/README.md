## Visualize OMNeT++ result data
This directory contains the raw result data in json format as well as the data plots. The `visualize.py` script is ony for plotting the end-to-end delays of the roboticArm and workstations.
For better plot diagrams you may need to adapt the scaling in the script. 

### How to extract and plot data:
1. Run the simulation (F7 for fast forward to the end)
2. Open the OMNeT++ result files generated in `simulations/example/results_*mechanism*_/General-#0.vec`
3. Let OMNeT++ generate a new analysis file (_General.anf_) and open it
4. In the _Browse Data_ window open the _Vectors (X/X)_ tab and search for the both _endToEndDelay:vector_ statistics
5. Mark both statistics (roboticArm and backupServer) and export the raw data with _Rightclick -> Export Data -> JSON ..._
6. Give the .json file a name, ignore the options and click _Finish_
7. Change the filename in `visualize.py` to the newly generated data file and run the script `./visualize.py`


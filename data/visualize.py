#!/usr/bin/env python
import matplotlib.pyplot as plt
import json
import numpy as np
import sys

if len(sys.argv) == 1:
	print("Usage: ./visualize.py <result_file.json>")
	exit(1)
else:
	filename = str(sys.argv[1])

class tv_vector:
	time: []
	delay: []
	label = ""

	def __init__(self, time, delay, label):
		self.time = time
		self.delay = delay
		self.label = label

# read file
f = open(filename,)
results = json.load(f)

# first key is a dynamic timestamp, cannot access via static name.
data = results[list(results.keys())[0]]
datasets = []

# extact data
for dataset in data['vectors']:
	datasets.append(tv_vector(dataset['time'], dataset['value'], dataset['module']))

# set size, axis labels and description
plt.figure(figsize=(15,8))
plt.xlabel('Time (s)')
plt.ylabel('Delay (s)')
plt.title('E2E Delay: BE vs PRIO traffic (' + filename + ')')

# plot data
for dataset in datasets:
	plt.plot(dataset.time, dataset.delay, label = dataset.label, marker = '+', linewidth = 0.5)

# set scaling
plt.xlim(0,0.20)
plt.ylim(0.020,0.0203)
plt.legend(prop={'size': 6})
plt.xticks(np.arange(0, 0.20, step=0.02))
plt.grid()
plt.show()

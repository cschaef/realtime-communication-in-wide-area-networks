# Various informations for deterministic networking

Moving a step forward in the quest for Deterministic Networks (DetNet)  
https://ieeexplore-ieee-org.ezproxy.fh-salzburg.ac.at/document/9142788

Time-sensitive and Deterministic Networking Whitepaper (Norman Finn, Huawei Technologies Co. Ltd, July 11, 2017)  
https://mentor.ieee.org/802.24/dcn/17/24-17-0020-00-sgtg-contribution-time-sensitive-and-deterministic-networking-whitepaper.pdf

DetNet Data Plane: IEEE 802.1 Time Sensitive Networking over MPLS  
https://tools.ietf.org/html/draft-ietf-detnet-tsn-vpn-over-mpls-03

Echtzeitfähigkeit durch Time-Sensitive Networking (TSN) (german)  
https://www.heise.de/select/ix/2018/1/1514513129464505

Deterministic Networking (detnet) on datatracker.ietf.org  
https://datatracker.ietf.org/wg/detnet/charter/

IETF Detnet Use-Cases  
https://datatracker.ietf.org/doc/html/draft-ietf-detnet-use-cases

IETF Deterministic Networking Architecture  
https://datatracker.ietf.org/doc/rfc8655/

Deterministic Networking Problem Statement  
https://datatracker.ietf.org/doc/rfc8557/

Deterministic Networking Use Cases  
https://datatracker.ietf.org/doc/rfc8578/

The Road Towards a Linux TSN Infrastrukture  
https://elinux.org/images/5/56/ELC-2018-USA-TSNonLinux.pdf

Deterministic Networking (DetNet) vs Time Sensitive Networking (TSN)  
https://www.net.in.tum.de/fileadmin/TUM/NET/NET-2019-10-1/NET-2019-10-1_15.pdf  

EtherCAT and TSN  
https://www.ethercat.org/download/documents/EtherCAT_and_TSN_Presentation.pdf  

TSN basierte automatisch etablierte Redundanz für deterministische Kommunikation  
https://link-springer-com.ezproxy.fh-salzburg.ac.at/chapter/10.1007/978-3-662-59895-5_1

Time-Sensitive Networking as the Communication Future of Industry 4.0  
https://www-sciencedirect-com.ezproxy.fh-salzburg.ac.at/science/article/pii/S2405896319326941

Hierarchical scheduling and real-time analysis for vehicular time-sensitive network  
https://ieeexplore-ieee-org.ezproxy.fh-salzburg.ac.at/document/9092590

A time-sensitive networking (tsn) simulation model based on omnet++  
https://ieeexplore-ieee-org.ezproxy.fh-salzburg.ac.at/document/8484302

Time-Sensitive Networking Task Group  
http://www.ieee802.org/1/pages/tsn.html

Schedulability analysis of Time-Sensitive Networks with scheduled traffic and preemption support  
https://www-sciencedirect-com.ezproxy.fh-salzburg.ac.at/science/article/pii/S0743731520303087

Omnetpp Website  
https://omnetpp.org/

INET Website  
https://inet.omnetpp.org/Introduction.html

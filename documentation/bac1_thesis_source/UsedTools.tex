% =========================================================
% Used Tools for simulation
% =========================================================
\chead[]{Melhorn P., Schaefer C.}
\section{Simulation}
The term framesize is understood in the following chapter as the size of the Ethernet frame including the physical part. Preamble and start of frame delimiter are therefor included. 

\subsection{Architecture}
The NeSTiNg framework comes with three preconfigured and adaptable simulations installed. For each simulation variant the same network architecture is used. The available simulations are as follows:
\begin{itemize}
	\item Strict Priority
	\item Gating
	\item Frame Preemption
\end{itemize}

Each variant was tested in advance to verify the functionality. 

\subsubsection{Network}
Figure \ref{fig:omnetpp_simulation_design_visual_complex} shows an overview of the used network architecture.
The five transmitters are connected to SwitchA, the 5 receivers to SwitchB. The Phasor Measurement Unit (PMU) sends frames to the Phasor Data Concentrator (PDC). Workstations (WS) send to backup server (BS). Workstations and backup servers are associated with each other based on the terminating digit. So WS1 sends to BS1, WS2 to BS2, and so on.
The devices are connected via a dedicated cable implementing the \textit{DatarateChannel} module which can be modified to change delay, packet-error rate, bit-error rate and the data rate.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.8\textwidth]{images/omnetpp_simulation_design_visual_complex.png}
    \caption{OMNeT++ Simulation Visual Design}
    \label{fig:omnetpp_simulation_design_visual_complex}
\end{figure}

\subsubsection{Devices}
\paragraphlb{workstations \& backupServers}
The workstations are equipped with a traffic generator module (\textit{VlanEtherTrafGen}), which is able to send scheduled Ethernet frames to a specific destination. As the scenarios are only used for a Layer 2 environment various modules have to be adapted to send genuine \textit{UDP} traffic. This is listed in section \ref{attachments}.

The data traffic between workstations and backup server is intended to simulate the cross-traffic occurring in a real packet-switched network. To match the randomness of real data streams as closely as possible, the transmission interval and also the packet size are chosen randomly. The upper and lower limits of the packet size are limited according to the Ethernet standard  \cite{802_1D_2004}. 

When selecting the transmission interval, it needs to be ensured that the cross traffic exceeds the possible throughput of the WAN link with certainty. For this purpose, the transmission delay is calculated for the maximum framesize (\textit{TDmax}). The limits of the interval for possible transmission intervals are set approximately 3 microseconds above, and 5 microseconds below the calculated \textit{TDmax}. The calculation of the transmission delay is shown in detail under point \textit{XY}. This creates a congestion situation, which results in a full queue and subsequent packet drops. 

\begin{align}
	\text{Workstation*:} \\
	\text{Transmission interval:}& \qquad 	t\in\mathbb{N},\,t\in[7, 15]\,us \\
	\text{Packetsize (L3-PDU):}& \qquad 	ps\in\mathbb{N},\,ps\in[46, 1500]\,Byte \\
	\text{Framesize (L2-PDU):}& \qquad	fs\in\mathbb{N},\,fs\in[76, 1530]\,Byte 
\end{align}

If the connecting switches would not support any TSN functionalities this traffic would delay the priority traffic from the PMU drastically and determinism would not be possible. As seen in the gating example (Figure \ref{fig:omnetpp_simulation_switchA_queueing}) traffic from queue \textit{5} and \textit{6} is on hold while real-time traffic from \textit{PMU} is immediately (excluding the processing delay) forwarded. 

\paragraphlb{PMU \& PDC}
The \textit{PMU} device also includes a traffic generating module (\textit{VlanEtherTrafGenSched}) which allows for more detailed scheduling options. This should represents the real-time traffic in this simulation and be forwarded by the switches with high priority.

The content of the SV packets is taken from the pcap file \textit{C37.118\_1PMU\_UDP.pcap} \cite{wireshark_sv_samples} and wrapped in the data packets of the simulation. The operation of the data correlation of the PDC requires that all SV packets are received at a defined time. The delay variation of the sample value packets should therefore be close to zero. 

\newpage
\paragraphlb{switchA \& switchB}
Both switches implement the module \textit{VlanEtherSwitchPreemptable} which supports basic TSN functionalities. The network devices are also interconnected with a serial cable from type \textit{DatarateChannel}. Each switch is able to preempt frames as well as to sort incoming traffic according to their PCP value in different queues. 
The forwarding decision is made on the basis of the MAC address. 

The Forwarding Information Base (FIB) of the switches is defined manually. The FIB contains information about the port and MAC address assignment in order to be able to forward frames accordingly. 

To better understand the queueing within the TSN switches, Figure \ref{fig:omnetpp_simulation_switchA_queueing} shows the frame queueing of the eth[3] interface of \textit{SwitchA}. The Figure shows an example of the internal queuing mechanisms using the \textit{Gating} method: the gates for frames with a PCP value of 0 to 6 are closed and frames are buffered, whilst frames of queue \textit{7} are immediately forwarded.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=1\textwidth]{images/omnetpp_simulation_tsn_switch_queueing_example.png}
	\caption{Frame queueing on the eth[3] interface of \textit{switchA}}
	\label{fig:omnetpp_simulation_switchA_queueing}
\end{figure}

\paragraphlb{Processing Delay}
To get the simulation as close as possible to real conditions, a processing delay is defined at the switches by which each frame is delayed. The processing delay results from the processing time of the switch per frame, in which the internal logics are run through.
\begin{align}
	Processing Delay:  D_{Proc(Switch)} = 5us
\end{align}

\paragraphlb{Queue}
An integer multiple of the maximum frame size is used as the capacity of the queue. A multiple of 100 frames is defined. As shown in the following calculation, the preamble must be subtracted from the maximum frame size to calculate the queuing capacity, since these 8 bytes are regenerated each time a frame is placed on the medium and therefore do not have to be stored temporarily.  

$\overline{Mean\;value}$ ... in the following document the mean value of an attribute type is marked by a dash above the respective attribute.

\begin{align}
	framesize_{max}: 1522\, Byte &= 1530\,Byte - 8\,Byte (preamble) \\
	queuesize_{max}: 1217600\,bit &= 100\,Frames * framesize_{max} * 8
\end{align}
From this, the maximum possible queuing delay related to only one queue can be approximated by calculating the transmission delay of the full queue. The preamble and the interframegap must be taken into account again for this.
\begin{align}
	frames_n &= \frac{QueueCapacity}{\overline{frameSize}}  \\
	D_{Q (congestion)} &= \frac{(\overline{frameSize} + preamble + interframegap) * frames_n * 8}{throughput}
\end{align}
%\begin{align}
%	number of frames &= Queue capacity / medium frameLength  \\
%	D_{Q (congestion)} &= ((medium frame length + preamble + interframegap) * 8 * number of frames) / throughput 
%\end{align}

\paragraphlb{Delay}
The \textbf{end-to-end delay $D_{EtE}$} is the time span from the moment the sender puts the first bit of a frame on the line until the moment the receiver receives the last bit of a frame. This is made up of the following delays.

\begin{itemize}
	\item \textbf{Queuing-Delay $D_{Q}$}: Time frames are cached in a queue before they are forwarded. Depends on cross traffic and queue capacity.
	\item \textbf{Processing-Delay $D_{Proc}$}: Processing time per frame per network element
	\item \textbf{Propagation Delay $D_{Prop}$}: The delay occurring due to the physically conditioned signal propagation time per medium
	\item \textbf{Transmission Delay $D_{T}$}: Time required to place a frame on the line. The dependence of frame length and link data rate is illustrated in the following formula.
\end{itemize}

\begin{align}
	D_{EtE} = \sum D_{T} + \sum D_{Prop} + \sum D_{Proc} + \sum D_{Q}
\end{align}
	
As Gigabit Ethernet is used throughout the simulation the datarate equals $1*10^9 Gb / s$. Therefor the transmission time can be calculated using following formula:

\begin{equation}
	D_{T} = \frac{Bytes * 8}{1*10^9}
\end{equation}

To dimension the experimental setup, the following transmission delays are calculated in advance.
\begin{align}
	CTmin \text{(Cross traffic min.)} &= 46 B + 30 B = 76B \\
	CTmax \text{(Cross traffic max.)} &= 1500 B + 30 B = 1530B \\
	SV \text{(Sample Value)} &= 76 B + 30 B = 106 B \\ 
	D_{T(CTmax)} &= 12.24 us \\
	D_{T(CTmin)} &= 0.608 us \\
	D_{T(SV)} &= 0.848 us 
\end{align}

Since all SV frames have the same frame length, the delay variation in this simulation depends only on the queuing delay. All other delay values in the simulation are assumed to be fixed.

\paragraphlb{Links}
The links that connect the devices to the switches are referred to below as the local link. The link between \textit{switchA} and \textit{switchB} is referred to as the WAN link. This is intended to simulate the wide area network. To focus on the TSN functionality of the switch, a packet loss and a delay variation of zero are assumed for the links. The other link properties are shown below:

\begin{equation}
	\begin{aligned}[c]
		&\text{Local Link:} \\
		&D_{Prop (LL)} &= 0.1us \\
		&Datarate &= 1Gb/s \\
	\end{aligned} 
	\qquad \qquad
	\begin{aligned}[c]
		&\text{WAN-Link:} \\
		&D_{Prop (WAN)} &= 20ms \\
		&Datarate &= 1Gb/s \\
	\end{aligned} 
\end{equation}

\subsubsection{Signal Mechanisms}
In OMNeT++ the use of \textit{simulation signals} allows for automatic generation of various statistics. Signals are emitted by components (as switches or end-devices) for certain events \cite{omnetpp_manual}. This can be used to capture timestamps whenever frames are sent from a device or received at an interface. The NeSTiNg framework already implements various signals in their simulations, which will be used later to show different end-to-end delay variations between the simulation scenarios.
% =========================================================
% Praktischer Teil
% =========================================================
\chead[]{Melhorn P., Schaefer C., Schnoell F.}
\section{Experiment}
In the following section, terms are derived that are necessary for the evaluation of the results. Then the results are analyzed for each simulation and corrected for each flow. Finally, the graphical results are analyzed and interpreted.

\paragraphlb{Interframegap (IFG)}
The IFG is the time that must elapse between two frames that are successively placed on the line. This time depends on the link data rate, as it is specified in Bit (b) or Byte (B) times. The standard size of the IFG is 96 Bit times. Since the minimum interframegap for GigabitEthernet is defined with 8 Byte times, the IFG used in the simulation is determined. The 12 Byte times, i.e. 96 Bit times, are used here. These are taken into account in all relevant calculations \cite{rfc2889}.

\paragraphlb{Average Frame Size (AFS)}
For the AFS, the results from the simulation result vectors are used. Since these are given without preamble, 8 Bytes are added to the extracted results. The values are given in Bytes.

\paragraphlb{Packets Transmitted ($P_{TX}$)}
These results were taken from the simulation result vectors.

\paragraphlb{Packets Recieved ($P_{RX}$)}
These results were taken from the simulation result vectors.

\paragraphlb{Average Transmission Interval (ATI)}
\vspace{-0.5cm}
\begin{figure}[!ht]
\begin{equation}
	\begin{aligned}[c]
		&\text{Calculation for Workstations:} \\
		&ATI = \frac{10s - 1us}{P_{TX}} \\
	\end{aligned}
	\qquad \qquad
	\begin{aligned}[c]
		&\text{Calculation for PMU:} \\
		&ATI = \frac{10s}{P_{TX}} \\
	\end{aligned}
\end{equation}
\caption*{The subtraction of one millisecond results from the staggered transmission start of the workstations.}
\end{figure}

\subsection{Simulation 1 - Best Effort}
In this simulation, the effects of best effort packet-switched networks on the transmission of SV packets should be shown. The effects should be visible in result parameters like packet loss and delay variation. For the simulation, the strict-priority sample is used. To simulate a best effort behavior, all workstations and also the PMU send frames with the PCP value 7. This means that at the outgoinig interface of SwitchA towards SwitchB, all frames are queued in the same queue. Since the transmission intervals of the cross traffic are dimensioned in relation to the frame size in such a way that the throughput of the WAN link is greatly exceeded, a congestion situation is created for almost the entire simulation time.

\subsubsection{Configuration}
Table \ref{tbl:table_be_sim_conf} shows the configuration setup for the two traffic flows (priority \& cross) as well as the simulation runtime. 

\begin{table}[!ht]
	\centering
	\begin{tabular}{l|ll|c}
			 & PCP & First Frame           & \multicolumn{1}{l}{Runtime}  \\
	\hline
	Priority Traffic & 7   & t0 (Simulation Start) & \multirow{2}{*}{10s}         \\
	Cross Traffic    & 7   & t0 + 1us               &
	\end{tabular}
	\caption{Best effort simulation configuration}
	\label{tbl:table_be_sim_conf}
\end{table}

\subsubsection{Results}

\paragraphlb{Workstations average transmission delay $D_{T WS (average)}$}
The average AFS is used to calculate the average transmission time of the workstations. The transmission delay of the SV frames is known.
\begin{align}
	\overline{AFS_{WS}} &= 802.5475\;B \\
	D_{T\;\overline{WS}} &= \frac{\overline{AFS_{WS}} * 8}{1*10^9} = 6.42\;us
\end{align}

\paragraphlb{Queuing Delay (DQ)}
In this section, the queuing delay is calculated assuming different packet sizes. This is needed to better estimate the range in which the queuing delay should move in congestion situations. \\

The \textbf{Queue Capacity} is static and capped at 1217600 Bits. Depending on the size of a frame, the queue can buffer more or less frames.
\begin{align}
	Queue \; Capacity: \frac{1217600\;b}{8} = 152200\;B 
\end{align}

The following equations are calculated with a full queue, using the respective frame size. In this case the preamble length was already subtracted from the frame size.
\\

Queuing Delay with the minimum frame size:
\begin{align}
	possible\; Frames&: \frac{152200\;B}{68\;B} = 2238.235\;Frames \\
	D_{Qmin}&: \frac{(68+8+12) * 8 * 2238.235}{1*10^9} = 1.576 ms
\end{align}

Queuing Delay with the average frame size.
The average frame size is calculated by creating the mean value of the cross traffic flows AFS.
\begin{align}
	possible\; Frames&: \frac{152200\;B}{794.55\;B} = 191.55\;Frames \\
	D_{Qavg}&:  \frac{(794.55+8+12) * 8 * 191.55}{1*10^9} = 1.248\;ms
\end{align}

Queuing Delay with the maximum frame size:
\begin{align}
	possible\; Frames&: \frac{152200\;B}{1522\;B} = 100\;Frames \\
	D_{Qmax}&:  \frac{(1522+8+12) * 8 * 100)}{1*10^9} = 1.234\;ms
\end{align}

\paragraphlb{End to end Delay}
As already mentioned above, the end-to-end delay can be calculated using following formula:
\begin{equation}
D_{EtE} = \sum D_T + \sum D_{Prop} + \sum D_{Proc} + \sum D_Q 
\end{equation}

The single delays have to be adapted according to the network architecture. For the calculations the average frame size over all workstations is used.
\begin{align}
&\sum D_T: D_{T(Transmitter)} + 2* D_{T(Switch)} = 3*D_T \\
&\sum D_{Prop}: 2* D_{Prop (LL)} + D_{Prop (WAN)} \\
&\sum D_{Proc}: 2* D_{Proc(Switch)} \\
&\sum D_Q: D_{Qmax} \dots \text{Since the simulation is in a congestion situation at the end.} 
\end{align}

The following equations show the calculated end-to-end delay in congestion situation, again using different frame size variations for the workstation (WS) traffic and static sizes for priority (PMU) traffic. \\

End-to-End delay with the minimum frame size:
\begin{align}
D_{EtE} &= \sum D_T + 20.0002ms + 10us + 1.5757ms \\
D_{EtE (WS)}  &= 19.26us + 20.0002ms + 10us + 1.5757 ms = 21.6052 ms \\
D_{EtE (PMU)} &= 2.544us + 20.0002ms + 10us + 1.5757 ms = 21.5884 ms
\end{align}

End-to-End delay with the average frame size:
\begin{align}
D_{EtE} &= \sum DT + 20.0002ms + 10us + 1.248ms \\
D_{EtE (WS)}  &= 19.26us + 20.0002ms + 10us + 1.248 ms= 21.2775 ms \\
D_{EtE (PMU)} &= 2.544us + 20.0002ms + 10us + 1.248 ms= 21.2607 ms 
\end{align}

End-to-End delay with the maximum frame size:
\begin{align}
D_{EtE} &= \sum DT + 20.0002ms + 10us + 1.234 ms \\
D_{EtE (WS)}  &= 19.26us + 20.0002ms + 10us + 1.234 ms = 21.2635 ms \\
D_{EtE (PMU)} &= 2.544us + 20.0002ms + 10us + 1.234 ms = 21.2467 ms 
\end{align}

\paragraphlb{Packets Transmitted corrected ($P_{TX\;corr.}$)}
Since the simulation has a hard limit at a simulation time of 10s and all transmitters transmit up to this limit, there is a bias in the results. This is due to the fact that all packets which are sent in the time simulation end minus end-to-end delay until simulation end cannot reach the receivers anymore. The value of the sent packets is therefore corrected by the correction factor calculated below. The calculation is based on the end-to-end delay value in the case of congestion and the average packet size. \\

Correction factor Workstation ($CF_{WS}$):
\begin{align}
	CF_{WS} &= \frac{D_{EtE\;(WS)\; average}}{\overline{ATI_{WS}}} \\
	CF_{WS} &= \frac{21.2775 ms}{11.001 us} = 1934.14  \\
	CF_{WS} &= 1935\;Packets
\end{align}

Correction factor PMU ($CF_{PMU}$):
\begin{align}
	CF_{PMU} &= \frac{D_{EtE\;(PMU)\; average}}{\overline{ATI_{PMU}}} \\
	CF_{PMU} &= \frac{21.2607 ms}{20 ms} = 1.063 \\
	CF_{PMU} &= 2\;Packets
\end{align}

The correct transmitted packets can now be calculated with following formula:
\begin{align}
	P_{TX\;corr.} = P_{TX} - CF
\end{align}

\paragraphlb{Packet loss}
The packetloss symbolizes the difference between 100 percent and the quotient of received and sent packets. It shows the percentage of sent packets that are not received.

\begin{align}
	PacketLoss = (1-\frac{P_{RX}}{P_{TX\;corr.}})) * 100
\end{align}

\subsubsection{Packet-Data}
\begin{table}[h]
	\centering
	\begin{tabular}{l|llllll}
		& \begin{tabular}[c]{@{}l@{}}ATI \\(us) \end{tabular} & \begin{tabular}[c]{@{}l@{}}AFS \\(Byte) \end{tabular} & $P_{tx}$  & $P_{tx corr.}$  & $P_{rx}$  & \begin{tabular}[c]{@{}l@{}}Packet \\Loss \end{tabular}  \\
\hline
WS1 $\rightarrow$ BS1  & 11.002                                              & 803.02                                                & 908,924         & 906,989               & 491,870         & 45.77\%                                                 \\
WS2 $\rightarrow$ BS2  & 10.999                                              & 802.08                                                & 909,208         & 907,273               & 492,126         & 45.76\%                                                 \\
WS3 $\rightarrow$ BS3  & 11.003                                              & 802.25                                                & 908,883         & 906,948               & 492,418         & 45.71\%                                                 \\
WS4 $\rightarrow$ BS4  & 11.000                                              & 802.84                                                & 909,102         & 907,167               & 491,785         & 45.79\%                                                 \\
PMU $\rightarrow$ PDC  & 20                                                  & 106                                                   & 500             & 498                   & 466             & 6.43\%
\end{tabular}
	\caption{Comparision of cross- \& priority traffic in strict priority mode}
\end{table}

The result shows that all flows of the cross traffic have a packetloss of almost 45 percent. This suggests that the queue is in a permanent congestion situation and therefore packets have to be discarded. The calculated random range of the transmission interval thus fulfills its purpose. The noticeably lower packet loss of the SV traffic can be attributed to the relatively smaller packet size. This increases the probability of finding space in an overloaded queue when a packet arrives.

\subsubsection{Graphical evaluation of the results}
In the following diagrams, the timing behavior of the packets of the different flows is examined. The abscissa shows the reception time of the packets, the ordinate the end-to-end delay. Each flow is represented by a different color. The assignment can be found in the legend of the respective diagram.

\begin{figure}[H]
    \centering
	\includegraphics[width=1\textwidth]{images/omnet_simulation1_result_general.png}
	\caption{Simulation 1: End to End delay - all workstations}
    \label{fig:omnet_simulation1_result_general}
\end{figure}

The diagram \ref{fig:omnet_simulation1_result_general} shows the queue running full and the subsequent transmission of packets around the calculated mean end-to-end delay when the queue is full. The fullness of the queue is shown by the approximately linear increase of the end-to-end delay after the time of 0.02 s. The average end-to-end delay is calculated from the average delay of the queue. The slope of the linear function results from the ratio of incoming and outgoing packet rate. In this case, the sharp increase is due to the configuration of the transmitters, whose transmission cycles are deliberately intended to exceed the capacity of the WAN link, and is also reflected in the previously calculated packet loss rate. The difference in the end-to-end delay between the first packet and the packets after the maximum queuing capacity has been reached corresponds to the previously calculated queuing delay when the queue is full.

\begin{figure}[H]
    \centering
	\includegraphics[width=1\textwidth]{images/omnet_simulation1_result_firstpacket.png}
	\caption{Simulation 1: End to End delay - first packets}
    \label{fig:omnet_simulation1_result_firstpacket}
\end{figure}

The graphic \ref{fig:omnet_simulation1_result_firstpacket} shows that the end-to-end delay of the first SV packet shown in blue corresponds exactly to the calculated end-to-end delay with an empty queue. The data packet in red shows the first packet of the cross traffic, which was put on the line 1us after the SV packet was sent, but has a higher transmission delay due to the larger frame size.

\begin{figure}[H]
    \centering
	\includegraphics[width=1\textwidth]{images/omnet_simulation1_result_queuingdelay.png}
	\caption{Simulation 1: End to End delay - variations of SV and BE traffic 1/2}
    \label{fig:omnet_simulation1_result_queuingdelay}
\end{figure}

The graph \ref{fig:omnet_simulation1_result_queuingdelay} shows that the calculated end-to-end delay (21.2775 ms) with average packet size and full queue also corresponds to the graphically displayed average transmission time. Despite the full queue, the end-to-end delay varies by about 40 us. On the one hand, this is due to the exact fill level of the queue when the packet arrives. On the other hand, it is due to the average size of the packets already queued. Since each packet to be sent must be preceded by the preamble and then the IFG must be waited for, small packets cause a greater delay than large packets for a given queue capacity.

\begin{figure}[H]
    \centering
	\includegraphics[width=1\textwidth]{images/omnet_simulation1_result_delayvariation.png}
	\caption{Simulation 1: End to End delay - variations of SV and BE traffic 2/2}
    \label{fig:omnet_simulation1_result_delayvariation}
\end{figure}

The Figure \ref{fig:omnet_simulation1_result_delayvariation} shows the delay variation of two consecutive SV packets and a section of the cross traffic.

\begin{figure}[H]
    \centering
	\includegraphics[width=1\textwidth]{images/omnet_simulation1_result_SVjitter.png}
	\caption{Simulation 1: End to End delay - SV jitter}
    \label{fig:omnet_simulation1_result_SVjitter}
\end{figure}

The plot \ref{fig:omnet_simulation1_result_SVjitter}  shows the delay variation of the sample-value packets over the simulation time. The cross traffic was hidden to improve the overview. The difference between upper and lower extreme value of the jitter is 22.9 us. In addition, it can be seen that the end-to-end delay of almost all SV packets is below the calculated mean end-to-end delay at congestion.  This is due to the smaller frame size and the associated lower transmission delay.


\subsection{Simulation 2 - Gating}
In this series of simulations, it is demonstrated how the previously shown behavior in terms of packet loss and jitter can be changed by deploying TSN functionality in the network nodes.

Gating is used as the TSN variant because it can achieve the lowest end-to-end delay and the lowest jitter for the traffic to be preferred with exact timing. This is obtained as a result of a queuing delay that tends to zero. Different PCP values are used to distinguish cross traffic from SV packets.

The PMU sets the PCP value 7, the workstations the PCP value 6. Thus, queue 7 and queue 6 can be handled in different gating timeslots in the TSN switch. Since there is no time synchronization or logic for defining the gating timeslots in the simulation, the time settings of the gating logic are calculated in a subsequent section and then configured in the simulation.

\subsubsection{Configuration}
Table \ref{tbl:table_gating_sim_conf} shows the configuration setup for the two traffic flows (priority \& cross) as well as the simulation runtime. 

\begin{table}[!ht]
	\centering
	\begin{tabular}{l|ll|c}
			 & PCP & First Frame           & \multicolumn{1}{l}{Runtime}  \\
	\hline
	Priority Traffic & 7   & t0 (Simulation Start) & \multirow{2}{*}{10s}         \\
	Cross Traffic    & 6   & t0 + 1us               &
	\end{tabular}
	\caption{Gating simulation configuration}
	\label{tbl:table_gating_sim_conf}
\end{table}


\paragraphlb{Gating settings in the switches}
The gating interval is selected analog to the cycle time of the SV packets. Since the first SV packet is sent at t0 of the simulation time, the gating cycle can start with the timeslot for queue 7. To simulate synchronization and resource reservation, subsequent delays must be considered in the timeslot. Since the PMU starts sending at t0, the transmission delay occurring there must be taken into account.
In addition, the propagation delay of the local link between PMU and SwitchA, the processing delay of the switch, and the transmission delay of SwitchA when sending to SwitchB must be added. It is advisable to dimension the gating timeslots as small as possible, otherwise resources are wasted or withheld from the best effort traffic. The calculation of the timeslot times is shown below.

\begin{align}
	CycleTime&: 20ms \\
	Timeslot\;PCP7: \; &2 * D_{T(SV)} + D_{Prop (LL)} + D_{Proc(Switch)} \\
			\; &2 * 0,848 us + 0.1us + 5us = 6,796us \approx 7us \\
	Timeslot\;PCP6:\;&20ms - 7us = 19993 us
\end{align}



\subsubsection{Results}

\paragraphlb{Workstations average transmission delay $D_{T WS (average)}$}
The average AFS is used to calculate the average transmission time of the workstations. The transmission delay of the SV frames is known.
\begin{align}
	\overline{AFS_{WS}} &= 802.5475\;B \\
	D_{T\;\overline{WS}} &= \frac{\overline{AFS_{WS}} * 8}{1*10^9} = 6.42\;us
\end{align}

\paragraphlb{End to end Delay}
Since the SV traffic has its own queue and the gating mechanism times it exactly according to the transmission cycle of the PMU, the queuing delay is only taken into account for the cross traffic when calculating the end-to-end delay.

\begin{align}
	D_{EtE} = \sum D_T + \sum D_{Prop} + \sum D_{Proc} + \sum D_Q 
\end{align}

End-to-End delay with for SV traffic:
\begin{align}
	D_{EtE} &= \sum D_T + 20.0002ms + 10us + \sum D_Q \\
	D_{EtE\;min.\;frameSize\;(PMU)} &= 2.544us + 20.0002ms + 10us + 0 = 20.0127ms
\end{align}

End-to-End delay for cross-traffic with minimum frame size:
\begin{align}
D_{EtE} &= \sum D_T + 20.0002ms + 10us + 1.5757 ms  \\
D_{EtE (WS)}  &= 19.26us + 20.0002ms + 10us + 1. 5757 ms = 21.6052 ms
\end{align}

End-to-End delay for cross-traffic with average frame size:
\begin{align}
D_{EtE} &= \sum DT + 20.0002ms + 10us + 1.248ms \\
D_{EtE (WS)}  &= 19.26us + 20.0002ms + 10us + 1.248 ms= 21.2775 ms 
\end{align}

End-to-End delay for cross-traffic with maximum frame size:
\begin{align}
D_{EtE} &= \sum DT + 20.0002ms + 10us + 1.234 ms \\
D_{EtE (WS)}  &= 19.26us + 20.0002ms + 10us + 1.234 ms = 21.2635 ms 
\end{align}

Correction factor Workstation ($CF_{WS}$):
\begin{align}
	CF_{WS} &= \frac{D_{EtE\;(WS)\; average}}{\overline{ATI_{WS}}} \\
	CF_{WS} &= \frac{21.2775 ms}{10.9995 us} = 1934.41  
	CF_{WS} &= 1935\;Packets
\end{align}

Correction factor PMU ($CF_{PMU}$):
\begin{align}
	CF_{PMU} &= \frac{D_{EtE\;(PMU)\; average}}{\overline{ATI_{PMU}}} \\
	CF_{PMU} &= \frac{20.0127 ms}{20 ms} = 1.0006 \\
	CF_{PMU} &= 1\;Packet
\end{align}

\begin{table}[]
	\begin{tabular}{l|llllll}
		& \begin{tabular}[c]{@{}l@{}}ATI \\(us) \end{tabular} & \begin{tabular}[c]{@{}l@{}}AFS \\(Byte) \end{tabular} & $P_{tx}$  & $P_{tx corr.}$  & $P_{rx}$  & \begin{tabular}[c]{@{}l@{}}Packet \\Loss \end{tabular}  \\
\hline
	WS1 $\rightarrow$ BS1 & 11       & 803.20     & 909,080         & 907,145               & 491,137         & 45.86\%     \\
	WS2 $\rightarrow$ BS2 & 10.998   & 801.95     & 909,288         & 907,353               & 491,908         & 45.79\%     \\
	WS3 $\rightarrow$ BS3 & 10.995   & 802.61     & 909,497         & 907,562               & 492,626         & 45.92\%     \\
	WS4 $\rightarrow$ BS4 & 11.005   & 802.43     & 908,715         & 906,780               & 491,669         & 45.78\%     \\
	PMU $\rightarrow$ PDC & 20       & 106        & 500            & 499                  & 499            & 0\%
\end{tabular}
	\caption{Comparision of cross- \& priority traffic in gating mode}
\end{table}

It can be seen that the packetloss of the cross traffic is about the same as what was achieved in simulation 1. However, the packet loss of the SV flow is zero percent and the calculated end-to-end delay is also at a constant low value due to the negligibility of the queuing delay. This evaluation not only shows that gating can keep the packet loss of a prioritized flow to zero in a heavily congested queue. It also shows that if the forwarding timeslot of the express traffic is calculated exactly, there is practically no effect on the cross traffic. This can be explained by the fact that with exact calculation no time resources are wasted on the outgoing link.

\subsubsection{Graphical evaluation of the results}
In the following diagrams, the timing behavior of the packets of the different flows is examined. The abscissa shows the reception time of the packets, the ordinate the end-to-end delay. Each flow is represented by a different color. The assignment can be found in the legend of the respective diagram.

\begin{figure}[H]
    \centering
	\includegraphics[width=1\textwidth]{images/omnet_simulation2_result_general.png}
	\caption{Simulation 2: End to End delay - all workstations}
    \label{fig:omnet_simulation2_result_general}
\end{figure}

The graph \ref{fig:omnet_simulation2_result_general} shows a similar behavior of the cross traffic as in simulation 1. However, the SV packets are shown here as a function parallel to the X-axis that is independent of the queuing delay as well as the congestion situation.

\begin{figure}[H]
    \centering
	\includegraphics[width=1\textwidth]{images/omnet_simulation2_result_firstpacket.png}
	\caption{Simulation 2: End to End delay - first packets}
    \label{fig:omnet_simulation2_result_firstpacket}
\end{figure}

Since the SV packets have their own queue with a coordinated forwarding timeslot, there is no variation in the end-to-end delay.

\begin{figure}[H]
    \centering
	\includegraphics[width=1\textwidth]{images/omnet_simulation2_result_SVjitter.png}
	\caption{Simulation 2: End to End delay - SV jitter}
    \label{fig:omnet_simulation2_result_SVjitter}
\end{figure}

This plot \ref{fig:omnet_simulation2_result_SVjitter} illustrates a constant end-to-end delay of SV traffic throughout the simulation. Cross traffic was again not shown for clarity. Furthermore, the simulated end-to-end delay corresponds to the value calculated beforehand.

\begin{figure}[H]
    \centering
	\includegraphics[width=1\textwidth]{images/omnet_simulation2_result_gating.png}
	\caption{Simulation 2: End to End delay - gating intervals}
    \label{fig:omnet_simulation2_result_gating}
\end{figure}

\begin{figure}[H]
    \centering
	\includegraphics[width=1\textwidth]{images/omnet_simulation2_result_gatingimpacts.png}
	\caption{Simulation 2: End to End delay - gating interval details}
    \label{fig:omnet_simulation2_result_gatingimpacts}
\end{figure}

The diagrams \ref{fig:omnet_simulation2_result_gating} and \ref{fig:omnet_simulation2_result_gatingimpacts} show that the forwarding timeslot of SV traffic temporarily increases the end-to-end delay of cross traffic. As a temporary increase of the delay, a value of approximately 10 us can be read from the graph. This is due to the gating time of the express traffic of 7us. The duration from the beginning of the forwarding timeslot of the PCP7 traffic at 0.06s until the regeneration of the end-to-end delay of the cross traffic corresponds approximately to the calculated queuing delay of a full queue with medium packet size. This is due to the fact that during the forwarding timeslot of the PCP7 traffic a full queue must wait and incoming packets are discarded.
